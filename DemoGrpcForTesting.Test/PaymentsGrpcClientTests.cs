﻿using DemoGrpcForTesting.Data;
using DemoGrpcForTesting.Test.Fakers;
using DemoGrpcForTesting.Test.PaymentsProto;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit.Abstractions;

namespace DemoGrpcForTesting.Test
{
    public class PaymentsGrpcClientTests : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly WebApplicationFactory<Program> _factory;

        public PaymentsGrpcClientTests(WebApplicationFactory<Program> factory, ITestOutputHelper helper)
        {
            _factory = factory.WithDefaultConfigurations(helper, services =>
            {
                var ef = services.Single(d => d.ServiceType == typeof(DbContextOptions<PaymentsDbContext>));
                services.Remove(ef);
                var dbName = Guid.NewGuid().ToString();
                services.AddDbContext<PaymentsDbContext>(options => options.UseInMemoryDatabase(dbName));
            });
        }

        [Fact]
        public async Task Bogus_Debut()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                var payments = new PaymentFaker().RuleFor(x => x.AccountNumber, "12345678-1").Generate(10);
                await context.Payments.AddRangeAsync(payments);
                await context.SaveChangesAsync();
            }
        }


        [Theory]
        [InlineData("11223344-1", 1000)]
        [InlineData("11223344-7", 2000)]
        [InlineData("98384124-2", 3000)]
        [InlineData("98384124-2", 100)]
        public async Task DepositAsync_WithValidRequest_ReturnOkAndDepositSaved(
            string accountNumber,
            double value
        )
        {
            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var response = await client.DepositAsync(new DepositRequest
            {
                AccountNumber = accountNumber,
                Value = value
            });

            using var scope = _factory.Services.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
            var deposits = await context.Payments.ToListAsync();

            Assert.NotEmpty(response.DepositId);

            var deposit = Assert.Single(deposits);

            Assert.Equal(response.DepositId, deposit.Id.ToString());
            Assert.Equal(accountNumber, deposit.AccountNumber);
            Assert.Equal((decimal)value, deposit.Value);
            Assert.Equal(DateTime.UtcNow, deposit.DebositedAt, TimeSpan.FromSeconds(3));
        }

        [Fact]
        public async Task DepositAsync_WithExistingDepositInTheSameDay_ThrowsFailedPrecondition()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                context.Payments.Add(new Payment
                {
                    AccountNumber = "12345668-1",
                    DebositedAt = DateTime.UtcNow,
                    Value = 1000
                });
                await context.SaveChangesAsync();
            }

            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var ex = await Assert.ThrowsAsync<RpcException>(() => client.DepositAsync(new DepositRequest
            {
                AccountNumber = "12345668-1",
                Value = 1000
            }).ResponseAsync);

            Assert.Equal(StatusCode.FailedPrecondition, ex.StatusCode);

            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                var deposits = await context.Payments.ToListAsync();
                Assert.Single(deposits);
            }
        }

        [Fact]
        public async Task DepositAsync_WithExistingDepositInTheSameDayButForDifferentAccount_ReturnOk()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                context.Payments.Add(new Payment
                {
                    AccountNumber = "12345668-2",
                    DebositedAt = DateTime.UtcNow,
                    Value = 1000
                });
                await context.SaveChangesAsync();
            }

            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            await client.DepositAsync(new DepositRequest
            {
                AccountNumber = "12345668-1",
                Value = 1000
            });

            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                var deposits = await context.Payments.ToListAsync();
                Assert.Equal(2, deposits.Count);
            }
        }

        [Fact]
        public async Task DepositAsync_WithExistingDepositInTheAnotherDay_ReturnOk()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                context.Payments.Add(new Payment
                {
                    AccountNumber = "12345668-1",
                    DebositedAt = DateTime.UtcNow.AddDays(-1),
                    Value = 1000
                });
                await context.SaveChangesAsync();
            }

            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            await client.DepositAsync(new DepositRequest
            {
                AccountNumber = "12345668-1",
                Value = 1000
            });

            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                var deposits = await context.Payments.ToListAsync();
                Assert.Equal(2, deposits.Count);
            }
        }

        //[Theory]
        //[InlineData(199.999, 199.99)]
        //[InlineData(199.9999999999, 199.99)]
        //public async Task DepositAsync_WithMultipleDecimalPlaces_OnlyTheFirst2DecimalDigitsAccepted(
        //    double inputValue,
        //    decimal resulValue
        //)
        //{
        //    var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

        //    var response = await client.DepositAsync(new DepositRequest
        //    {
        //        AccountNumber = "12345678-1",
        //        Value = inputValue
        //    });

        //    using var scope = _factory.Services.CreateScope();
        //    var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
        //    var deposit = await context.Payments.SingleAsync(x => x.Id == Guid.Parse(response.DepositId));

        //    Assert.Equal(resulValue, deposit.Value);
        //}

        [Theory]
        [InlineData("11223344-", 1000)]
        [InlineData("11223344--", 1000)]
        [InlineData("11223344-+", 1000)]
        [InlineData("11a23344-1", 1000)]
        [InlineData("1123344-1", 1000)]
        [InlineData("112334--1", 1000)]
        [InlineData("112334-1", 1000)]
        [InlineData("1123344-11", 1000)]
        [InlineData("11223344-1", 99.9999999999)]
        public async Task DepositAsync_WithInvalidRequest_ThrowsInvalidArgumentException(
            string accountNumber,
            double value
        )
        {
            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var ex = await Assert.ThrowsAsync<RpcException>(() => client.DepositAsync(new DepositRequest
            {
                AccountNumber = accountNumber,
                Value = value
            }).ResponseAsync);

            Assert.Equal(StatusCode.InvalidArgument, ex.StatusCode);
        }
    }
}
