namespace DemoGrpcForTesting.Test
{
    public class CreateCustomerTestsOld
    {
        [Theory]
        [InlineData("John", 18)]
        [InlineData("Ahmed", 25)]
        [InlineData("Bin", 100)]
        [InlineData("Any other customer name", 130)]
        public void IfCustomerAgeIsBetween17And130AndNameIsMoreThan2Chars_ItShouldBeValid(string name, int age)
        {
            var service = new CustomersServiceOld();

            var customer = new Customer { Name = name, Age = age };

            var response = service.ValidateCustomer(customer);

            if (response.IsValidCustomer == false)
            {
                throw new Exception("Customer is not valid");
            }
        }

        [Theory]
        [InlineData(null, 20)]
        [InlineData("", 20)]
        [InlineData(" ", 20)]
        [InlineData("a", 20)]
        [InlineData("ab", 20)]
        [InlineData(" ab ", 20)]
        [InlineData("Ahmed", 17)]
        [InlineData("Ahmed", 10)]
        [InlineData("Ahmed", 1)]
        [InlineData("Ahmed", 131)]
        [InlineData("Ahmed", 140)]
        [InlineData("Ahmed", 500)]
        [InlineData("Ahmed", 1000)]
        public void IfCustomerAgeIsBelow18OrAbove130_ItShouldNotBeValid(string name, int age)
        {
            var service = new CustomersServiceOld();

            var customer = new Customer { Name = name, Age = age };

            var response = service.ValidateCustomer(customer);

            if (response.IsValidCustomer == true)
            {
                throw new Exception("Customer is valid");
            }
        }
    }
}