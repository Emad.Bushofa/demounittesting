﻿using Bogus;
using DemoGrpcForTesting.Data;

namespace DemoGrpcForTesting.Test.Fakers
{
    public class PaymentFaker : Faker<Payment>
    {
        public PaymentFaker()
        {
            RuleFor(x => x.Id, f => f.Random.Guid());
            RuleFor(x => x.AccountNumber, f => $"{f.Random.Int(10000000, 99999999)}-{f.Random.Int(0, 9)}");
            RuleFor(x => x.DebositedAt, f => f.Date.Past());
            RuleFor(x => x.Value, f => f.Finance.Amount());
        }
    }
}
