﻿using DemoGrpcForTesting.Test.CustomersProto;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit.Abstractions;

namespace DemoGrpcForTesting.Test
{
    public class CustomersGrpcClientTests : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly WebApplicationFactory<Program> _factory;

        public CustomersGrpcClientTests(WebApplicationFactory<Program> factory, ITestOutputHelper helper)
        {
            _factory = factory.WithDefaultConfigurations(helper);
        }


        [Theory]
        [InlineData("John", 18)]
        [InlineData("Ahmed", 25)]
        [InlineData("Bin", 100)]
        [InlineData("Any other customer name", 130)]
        public async Task IfCustomerAgeIsBetween17And130AndNameIsMoreThan2Chars_ItShouldBeValid(string name, int age)
        {
            var client = new Customers.CustomersClient(_factory.CreateGrpcChannel());

            var response = await client.ValidateCustomerAsync(new ValidateCustomerRequest
            {
                Age = age,
                Name = name,
                Address = "Some"
            });

            Assert.True(response.IsValid);
        }

        [Theory]
        [InlineData("", 20)]
        [InlineData(" ", 20)]
        [InlineData("a", 20)]
        [InlineData("ab", 20)]
        [InlineData(" ab ", 20)]
        [InlineData("Ahmed", 17)]
        [InlineData("Ahmed", 10)]
        [InlineData("Ahmed", 1)]
        [InlineData("Ahmed", 131)]
        [InlineData("Ahmed", 140)]
        [InlineData("Ahmed", 500)]
        [InlineData("Ahmed", 1000)]
        public async Task IfCustomerAgeIsBelow18OrAbove130_ItShouldNotBeValid(string name, int age)
        {
            var client = new Customers.CustomersClient(_factory.CreateGrpcChannel());

            var response = await client.ValidateCustomerAsync(new ValidateCustomerRequest
            {
                Age = age,
                Name = name,
                Address = "Some"
            });

            Assert.False(response.IsValid);
        }
    }
}
