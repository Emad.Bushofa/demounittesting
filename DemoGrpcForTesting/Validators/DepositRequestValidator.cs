﻿using DemoGrpcForTesting.PaymentsProto;
using FluentValidation;

namespace DemoGrpcForTesting.Validators
{
    public class DepositRequestValidator : AbstractValidator<DepositRequest>
    {
        public DepositRequestValidator()
        {
            RuleFor(x => x.AccountNumber).Matches(@"^\d{8}-\d{1}$");
            RuleFor(x => x.Value).GreaterThanOrEqualTo(100);
        }
    }
}
