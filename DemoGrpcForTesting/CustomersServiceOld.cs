﻿namespace DemoGrpcForTesting
{
    public class CustomersServiceOld
    {
        public Response ValidateCustomer(Customer customer)
        {
            if (customer.Age < 18 || customer.Age > 130)
            {
                return new Response { IsValidCustomer = false };
            }

            if (customer.Name is null || customer.Name.Trim().Length < 3)
            {
                return new Response() { IsValidCustomer = false };
            }

            return new Response { IsValidCustomer = true };
        }
    }

    public class Customer
    {
        public string? Name { get; set; }
        public int Age { get; set; }
    }

    public class Response
    {
        public bool IsValidCustomer { get; set; }
    }
}
