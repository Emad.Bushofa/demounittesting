﻿namespace DemoGrpcForTesting.Data
{
    public class Payment
    {
        public Guid Id { get; set; }
        public string? AccountNumber { get; set; }
        public decimal Value { get; set; }
        public DateTime DebositedAt { get; set; }
    }
}
