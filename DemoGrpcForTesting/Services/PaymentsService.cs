﻿using DemoGrpcForTesting.Data;
using DemoGrpcForTesting.PaymentsProto;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;

namespace DemoGrpcForTesting.Services
{
    public class PaymentsService : Payments.PaymentsBase
    {
        private readonly PaymentsDbContext _context;

        public PaymentsService(PaymentsDbContext context)
        {
            _context = context;
        }

        public override async Task<DepositResponse> Deposit(DepositRequest request, ServerCallContext context)
        {
            if (await _context.Payments.AnyAsync(x => x.AccountNumber == request.AccountNumber && x.DebositedAt.Date == DateTime.Today))
            {
                throw new RpcException(new Status(StatusCode.FailedPrecondition, ""));
            }

            var payment = new Payment()
            {
                AccountNumber = request.AccountNumber,
                DebositedAt = DateTime.UtcNow,
                Value = (decimal)request.Value
            };

            await _context.Payments.AddAsync(payment);
            await _context.SaveChangesAsync();

            return new DepositResponse()
            {
                DepositId = payment.Id.ToString()
            };
        }
    }
}
