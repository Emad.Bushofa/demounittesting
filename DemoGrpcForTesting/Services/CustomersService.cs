using DemoGrpcForTesting.CustomersProto;
using Grpc.Core;

namespace DemoGrpcForTesting.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        public override Task<ValidateCustomerResponse> ValidateCustomer(ValidateCustomerRequest request, ServerCallContext context)
        {
            if (request.Address.Length < 3 || request.Address.Length > 10)
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Address length should be between 3 and 10 characters"));

            var customers = new CustomersServiceOld();
            var customer = new Customer
            {
                Name = request.Name,
                Age = request.Age
            };

            var response = customers.ValidateCustomer(customer);
            return Task.FromResult(new ValidateCustomerResponse { IsValid = response.IsValidCustomer });
        }
    }
}
